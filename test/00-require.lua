#!/usr/bin/env lua

require 'Test.Assertion'

plan(16)

if not require_ok 'ChartPlot' then
    BAIL_OUT "no lib"
end

local m = require 'ChartPlot'
is_table( m, 'module' )
equals( m, package.loaded['ChartPlot'], 'package.loaded' )

local o = m.new()
is_table( o, 'instance' )
is_function( o.setData, 'meth setData' )
is_function( o.setTag, 'meth setTag' )
is_function( o.setGraphOptions, 'meth setGraphOptions' )
is_function( o.getBounds, 'meth getBounds' )
is_function( o.draw, 'meth draw' )
is_function( o.getGDobject, 'meth getGDobject' )
is_function( o.data2px, 'meth data2px' )

equals( m._NAME, 'ChartPlot', "_NAME" )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'plot two dimensional data', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )
